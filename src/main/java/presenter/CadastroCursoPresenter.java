/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presenter;

import dao.AlunoDAO;
import dao.CursoDAO;
import java.util.List;
import javax.persistence.NoResultException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.Aluno;
import model.Curso;
import org.apache.commons.lang3.StringUtils;
import view.CadastroCursoView;

/**
 *
 * @author BZR4
 */
public class CadastroCursoPresenter {
    private CadastroCursoView view;
    private static final String NUMBER_FORMAT_EXCEPTION = "Apenas números são permitidos para código de curso.";
    private static final String CODIGO_NAO_PREENCHIDO = "Código é obrigatório!";
    private static final String DESCRICAO_NAO_PREENCHIDA = "Nome é obrigatório.";
    private static final String CODIGO_E_DESCRICAO_NAO_PREENCHIDOS = "Código e nome são atributos obrigatórios.";
    private static final String CURSO_CRIADO = "Novo curso registrado com sucesso.";
    private static final String CURSO_REMOVIDO = "Curso removido com sucesso.";
    
    public CadastroCursoPresenter(CadastroCursoView view){
        this.view = view;
    }
    
    public CadastroCursoView getView(){
        return this.view;
    }
    
    
    public void saveCurso(){
        try {
            String id = view.getjTextFieldIdCurso().getText();
            String descricao = view.getjTextFieldDescricaoCurso().getText().trim();
            String ementa = view.getjTextAreaEmentaCurso().getText().trim();
            validadeFields(id, descricao);
            Integer codigo = Integer.parseInt(id);                        
            CursoDAO cursoDAO = CursoDAO.getInstance();
            Curso curso = new Curso(codigo, descricao, ementa);
            cursoDAO.persist(curso);
            loadTableCursos();
            JOptionPane.showMessageDialog(view, CURSO_CRIADO, "Mensagem", 1);            
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(view, NUMBER_FORMAT_EXCEPTION);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(view, e.getLocalizedMessage());
        }
    }
    
    public void deleteCurso(){
        try {
            String id = view.getjTextFieldIdCurso().getText();  
            validadeCodigo(id);
            Integer codigo = Integer.parseInt(id);            
            CursoDAO cursoDAO = CursoDAO.getInstance();
            Curso curso = cursoDAO.findByCodigo(codigo);
            cursoDAO.remove(curso);     
            loadTableCursos();
            JOptionPane.showMessageDialog(view, CURSO_REMOVIDO, "Mensagem", 1); 
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(view, NUMBER_FORMAT_EXCEPTION);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(view, e.getLocalizedMessage());
        }
    }
    
    public void updateCurso(){
        try {
            String id = view.getjTextFieldIdCurso().getText();
            String descricao = view.getjTextFieldDescricaoCurso().getText().trim();
            String ementa = view.getjTextAreaEmentaCurso().getText().trim();
            validadeFields(id, descricao);
            Integer codigo = Integer.parseInt(id);
            CursoDAO cursoDAO = CursoDAO.getInstance();
            Curso curso = cursoDAO.findByCodigo(codigo);
            curso.setDescricao(descricao);
            curso.setEmenta(ementa);
            cursoDAO.merge(curso);    
            loadTableCursos();
            JOptionPane.showMessageDialog(view, "Curso atualizado com sucesso", "Mensagem", 1); 
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(view, NUMBER_FORMAT_EXCEPTION);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(view, e.getLocalizedMessage());
        }
    }
    
    public void findCurso(){
        try {
            String id = view.getjTextFieldIdCurso().getText();   
            validadeCodigo(id);
            Integer codigo = Integer.parseInt(id);
            CursoDAO cursoDAO = CursoDAO.getInstance();
            Curso curso = cursoDAO.findByCodigo(codigo);
            view.getjTextFieldIdCurso().setText(curso.getCodigo().toString());
            view.getjTextFieldDescricaoCurso().setText(curso.getDescricao());
            view.getjTextAreaEmentaCurso().setText(curso.getEmenta());
            
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(view, NUMBER_FORMAT_EXCEPTION);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(view, e instanceof NoResultException ? "Curso não localizado." :e.getLocalizedMessage());
        }
    }
    
    public void loadTableCursos(){
        DefaultTableModel model = (DefaultTableModel) view.getjTableCursos().getModel();
        
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
        
        CursoDAO dao = CursoDAO.getInstance();
        List<Curso> cursos = dao.findAll();
        
        for(int i = 0; i < cursos.size(); i++){
            model.addRow(new Object[0]);
            model.setValueAt(cursos.get(i).getCodigo(), i, 0);
            model.setValueAt(cursos.get(i).getDescricao(), i, 1);
            model.setValueAt(cursos.get(i).getEmenta(), i, 2);
        }
    }
    
    private void validadeFields(String codigo, String descricao) throws Exception{
        if (StringUtils.isBlank(codigo) && StringUtils.isBlank(descricao)) {            
            throw new Exception(CODIGO_E_DESCRICAO_NAO_PREENCHIDOS);
        }
        
        if (StringUtils.isBlank(codigo)) {            
            throw new Exception(CODIGO_NAO_PREENCHIDO);
        }
                
        if (StringUtils.isBlank(descricao)) {            
            throw new Exception(DESCRICAO_NAO_PREENCHIDA);
        }
    }
    
    private void validadeCodigo(String codigo) throws Exception{
        if (StringUtils.isBlank(codigo)) {            
            throw new Exception(CODIGO_NAO_PREENCHIDO);
        }       
    }

    public void cleanFields() {
        view.getjTextFieldIdCurso().setText("");
        view.getjTextFieldDescricaoCurso().setText("");
        view.getjTextAreaEmentaCurso().setText("");
    }
}
