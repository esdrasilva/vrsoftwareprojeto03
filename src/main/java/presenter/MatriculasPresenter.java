/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presenter;

import dao.AlunoDAO;
import dao.CursoDAO;
import dao.MatriculaDAO;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.Aluno;
import model.Curso;
import model.CursoAluno;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import view.MatriculasView;

/**
 *
 * @author BZR4
 */
public class MatriculasPresenter {
    
    private MatriculasView view;
    private static final String NUMBER_FORMAT_EXCEPTION = "Apenas números são permitidos para código de aluno.";
    private static final String CODIGO_NAO_PREENCHIDO = "Código é obrigatório!";
    private static final String NOME_NAO_PREENCHIDO = "Nome é obrigatório.";
    private static final String CODIGO_E_NOME_NAO_PREENCHIDOS = "Código e nome são atributos obrigatórios.";
    
    
    public MatriculasPresenter(MatriculasView view){
        this.view = view;
    }

    public MatriculasView getView() {
        return this.view;
    }
    
    public void saveMatricula(){
        try {
            String id = view.getjTextFieldCodigo().getText();
            String nome = view.getjTextFieldNome().getText();
            validadeFields(id, nome);
            Integer codigo = Integer.parseInt(id);                        
            AlunoDAO alunoDAO = AlunoDAO.getInstance();
            Aluno aluno = alunoDAO.findByCodigo(codigo);
            CursoDAO cursoDAO = CursoDAO.getInstance();
            Curso curso = cursoDAO.findByDescricao(view.getjComboBoxCurso().getSelectedItem().toString());
            CursoAluno cursoAluno = new CursoAluno();
            String matricula = view.getjTextFieldNumberMatricula().getText();
            validadeCodigo(matricula);
            Integer numberMatricula = Integer.parseInt(matricula);
            cursoAluno.setCodigo(numberMatricula);
            cursoAluno.setCodigoCurso(curso);
            cursoAluno.setCodigoAluno(aluno);
            
            MatriculaDAO matriculaDAO = MatriculaDAO.getInstance();
            matriculaDAO.persist(cursoAluno);
                        
            loadViewDataMatriculas();
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(view, NUMBER_FORMAT_EXCEPTION);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(view, e.getLocalizedMessage());
        }
    }
    
    public void deleteMatricula(){
        try {
            String matricula = view.getjTextFieldNumberMatricula().getText();
            validadeCodigo(matricula);
            Integer codigo = Integer.parseInt(matricula);               
            MatriculaDAO matriculaDAO = MatriculaDAO.getInstance();
            CursoAluno cursoAluno = matriculaDAO.findByCodigo(codigo);
            matriculaDAO.remove(cursoAluno);            
            loadViewDataMatriculas();
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(view, NUMBER_FORMAT_EXCEPTION);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(view, e.getLocalizedMessage());
        }
    }
       
    
    public void findMatricula(){
        try {
            String id = view.getjTextFieldCodigo().getText();            
            validadeCodigo(id);
            Integer codigo = Integer.parseInt(id);
            AlunoDAO alunoDAO = AlunoDAO.getInstance();
            Aluno aluno = alunoDAO.findByCodigo(codigo);
            if (ObjectUtils.isNotEmpty(aluno)) {
                view.getjTextFieldCodigo().setText(aluno.getCodigo().toString());
                view.getjTextFieldNome().setText(aluno.getNome());
            }else{
                JOptionPane.showMessageDialog(view, "Aluno não localizado!");
            }
            
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(view, NUMBER_FORMAT_EXCEPTION);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(view, e.getLocalizedMessage());
        }
    }
    
    public void loadMatricula(String matricula){
        try {
            DefaultTableModel model = (DefaultTableModel) view.getjTableMatriculas().getModel();

           for (int i = model.getRowCount() - 1; i >= 0; i--) {
               model.removeRow(i);
           }

           MatriculaDAO dao = MatriculaDAO.getInstance();
           validadeCodigo(matricula);
           Integer numberMatricula = Integer.parseInt(matricula);
           CursoAluno cursoAluno = dao.findByCodigo(numberMatricula);

           model.addRow(new Object[0]);
           model.setValueAt(cursoAluno.getCodigo(), 0, 0);
           model.setValueAt(cursoAluno.getCodigoCurso().getDescricao(), 0, 1);
           model.setValueAt(cursoAluno.getCodigoAluno().getNome(), 0, 2);
           model.setValueAt(cursoAluno.getCodigoAluno().getCodigo(), 0, 3);   
        } catch (Exception e) {
             JOptionPane.showMessageDialog(view, e.getLocalizedMessage());   
        }
     
    }
    
    public void loadViewDataMatriculas(){
        DefaultTableModel model = (DefaultTableModel) view.getjTableMatriculas().getModel();
        
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
        
        MatriculaDAO dao = MatriculaDAO.getInstance();
        CursoDAO cursoDAO = CursoDAO.getInstance();
        List<Curso> cursos = cursoDAO.findAll();
        
        List<String> listCursos = new ArrayList<>();
        cursos.forEach(c -> listCursos.add(c.getDescricao()));
        
        DefaultComboBoxModel boxModel = new DefaultComboBoxModel(listCursos.toArray());
        view.getjComboBoxCurso().setModel(boxModel);
        
        List<CursoAluno> matriculas = dao.findAll();
        CursoAluno matricula = null;
        
        for(int i = 0; i < matriculas.size(); i++){
            matricula = matriculas.get(i);
            model.addRow(new Object[0]);
            model.setValueAt(matricula.getCodigo(), i, 0);
            model.setValueAt(matricula.getCodigoCurso().getDescricao(), i, 1);
            model.setValueAt(matricula.getCodigoAluno().getNome(), i, 2);
            model.setValueAt(matricula.getCodigoAluno().getCodigo(), i, 3);
        }        
    }
    
    public void loadMatriculasByCurso(){
        DefaultTableModel model = (DefaultTableModel) view.getjTableMatriculas().getModel();
        
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
        
        MatriculaDAO dao = MatriculaDAO.getInstance();
        CursoDAO cursoDAO = CursoDAO.getInstance();
        Curso curso = cursoDAO.findByDescricao(view.getjComboBoxCurso().getSelectedItem().toString());
                
        List<CursoAluno> matriculas = dao.findAllByCodigo(curso.getCodigo());
        CursoAluno matricula = null;
        
        for(int i = 0; i < matriculas.size(); i++){
            matricula = matriculas.get(i);
            model.addRow(new Object[0]);
            model.setValueAt(matricula.getCodigo(), i, 0);
            model.setValueAt(matricula.getCodigoCurso().getDescricao(), i, 1);
            model.setValueAt(matricula.getCodigoAluno().getNome(), i, 2);
            model.setValueAt(matricula.getCodigoAluno().getCodigo(), i, 3);
        }
    }
    
    private void validadeFields(String codigo, String nome) throws Exception{
        if (StringUtils.isBlank(codigo) && StringUtils.isBlank(nome)) {            
            throw new Exception(CODIGO_E_NOME_NAO_PREENCHIDOS);
        }
        
        if (StringUtils.isBlank(codigo)) {            
            throw new Exception(CODIGO_NAO_PREENCHIDO);
        }
                
        if (StringUtils.isBlank(nome)) {            
            throw new Exception(NOME_NAO_PREENCHIDO);
        }
    }
    
    private void validadeCodigo(String codigo) throws Exception{
        if (StringUtils.isBlank(codigo)) {            
            throw new Exception(CODIGO_NAO_PREENCHIDO);
        }       
    }    

    public void fillAluno() {
        Integer row = view.getjTableMatriculas().getSelectedRow();
        Integer codigo = (Integer) view.getjTableMatriculas().getValueAt(row, 3);
        String nome = (String) view.getjTableMatriculas().getValueAt(row, 2);        
        view.getjTextFieldCodigo().setText(codigo.toString());
        view.getjTextFieldNome().setText(nome);
    }
}
