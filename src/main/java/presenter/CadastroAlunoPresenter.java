/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presenter;

import dao.AlunoDAO;
import java.util.List;
import java.util.Objects;
import javax.persistence.NoResultException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.Aluno;
import org.apache.commons.lang3.StringUtils;
import view.CadastroAlunoView;

/**
 *
 * @author BZR4
 */
public class CadastroAlunoPresenter {
    
    private CadastroAlunoView view;
    private static final String NUMBER_FORMAT_EXCEPTION = "Apenas números são permitidos para código de aluno.";
    private static final String CODIGO_NAO_PREENCHIDO = "Código é obrigatório!";
    private static final String NOME_NAO_PREENCHIDO = "Nome é obrigatório.";
    private static final String CODIGO_E_NOME_NAO_PREENCHIDOS = "Código e nome são atributos obrigatórios.";
    private static final String ALUNO_CRIADO = "Novo aluno registrado com sucesso.";
    private static final String ALUNO_REMOVIDO = "Aluno removido com sucesso.";
    
    
    public CadastroAlunoPresenter(CadastroAlunoView view){
        this.view = view;
    }

    public CadastroAlunoView getView() {
        return this.view;
    }
    
    public void saveAluno(){
        try {
            String id = view.getjTextFieldId().getText();
            String nome = view.getjTextFieldNome().getText().trim();
            validadeFields(id, nome);
            Integer codigo = Integer.parseInt(id);                        
            AlunoDAO alunoDAO = AlunoDAO.getInstance();
            Aluno aluno = new Aluno(codigo, nome);
            alunoDAO.persist(aluno);            
            loadTableAlunos();
            JOptionPane.showMessageDialog(view, ALUNO_CRIADO, "Mensagem", 1);
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(view, NUMBER_FORMAT_EXCEPTION);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(view, e.getLocalizedMessage());
        }
    }
    
    public void deleteAluno(){
        try {
            String id = view.getjTextFieldId().getText();
            String nome = view.getjTextFieldNome().getText().trim();
            validadeCodigo(id);
            Integer codigo = Integer.parseInt(id);               
            AlunoDAO alunoDAO = AlunoDAO.getInstance();
            Aluno aluno = alunoDAO.findByCodigo(codigo);
            alunoDAO.remove(aluno);     
            loadTableAlunos();
            JOptionPane.showMessageDialog(view, ALUNO_REMOVIDO, "Mensagem", 1);
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(view, NUMBER_FORMAT_EXCEPTION);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(view, e.getLocalizedMessage());
        }
    }
    
    public void updateAluno(){
        try {
            String id = view.getjTextFieldId().getText();
            String nome = view.getjTextFieldNome().getText().trim();
            validadeFields(id, nome);
            Integer codigo = Integer.parseInt(id);            
            AlunoDAO alunoDAO = AlunoDAO.getInstance();
            Aluno aluno = alunoDAO.findByCodigo(codigo);
            aluno.setNome(nome);
            alunoDAO.merge(aluno);  
            loadTableAlunos();
            JOptionPane.showMessageDialog(view, "Dados atualizados com sucesso", "Mensagem", 1);
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(view, NUMBER_FORMAT_EXCEPTION);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(view, e.getLocalizedMessage());
        }
    }
    
    public void findAluno(){
        try {
            String id = view.getjTextFieldId().getText();
            String nome = view.getjTextFieldNome().getText().trim();
            validadeCodigo(id);
            Integer codigo = Integer.parseInt(id);
            AlunoDAO alunoDAO = AlunoDAO.getInstance();
            Aluno aluno = alunoDAO.findByCodigo(codigo);            
            view.getjTextFieldId().setText(aluno.getCodigo().toString());
            view.getjTextFieldNome().setText(aluno.getNome());
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(view, NUMBER_FORMAT_EXCEPTION);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(view, e instanceof NoResultException ? "Aluno não localizado." :e.getLocalizedMessage());
        }
    }
    
    public void loadTableAlunos(){
        DefaultTableModel model = (DefaultTableModel) view.getjTableAlunos().getModel();        
        
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
        
        AlunoDAO dao = AlunoDAO.getInstance();
        List<Aluno> alunos = dao.findAll();
        
        for(int i = 0; i < alunos.size(); i++){
            model.addRow(new Object[0]);
            model.setValueAt(alunos.get(i).getCodigo(), i, 0);
            model.setValueAt(alunos.get(i).getNome(), i, 1);
        }
    }
    
    private void validadeFields(String codigo, String nome) throws Exception{
        if (StringUtils.isBlank(codigo) && StringUtils.isBlank(nome)) {            
            throw new Exception(CODIGO_E_NOME_NAO_PREENCHIDOS);
        }
        
        if (StringUtils.isBlank(codigo)) {            
            throw new Exception(CODIGO_NAO_PREENCHIDO);
        }
                
        if (StringUtils.isBlank(nome)) {            
            throw new Exception(NOME_NAO_PREENCHIDO);
        }
    }
    
    private void validadeCodigo(String codigo) throws Exception{
        if (StringUtils.isBlank(codigo)) {            
            throw new Exception(CODIGO_NAO_PREENCHIDO);
        }       
    }

    public void cleanFields() {
        view.getjTextFieldId().setText("");
            view.getjTextFieldNome().setText("");
    }
}
