/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author BZR4
 */
@Entity
@Table(name = "curso_aluno")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CursoAluno.findAll", query = "SELECT c FROM CursoAluno c ORDER BY c.codigoCurso.descricao, c.codigoAluno.nome ASC"),
    @NamedQuery(name = "CursoAluno.findAllByCodigo", query = "SELECT c FROM CursoAluno c WHERE c.codigoCurso.codigo = :codigo"),
    @NamedQuery(name = "CursoAluno.findByCodigo", query = "SELECT c FROM CursoAluno c WHERE c.codigo = :codigo")})
public class CursoAluno implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "codigo")
    private Integer codigo;
    @JoinColumn(name = "codigo_aluno", referencedColumnName = "codigo")
    @ManyToOne(optional = false)
    private Aluno codigoAluno;
    @JoinColumn(name = "codigo_curso", referencedColumnName = "codigo")
    @ManyToOne(optional = false)
    private Curso codigoCurso;

    public CursoAluno() {
    }

    public CursoAluno(Integer codigo) {
        this.codigo = codigo;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public Aluno getCodigoAluno() {
        return codigoAluno;
    }

    public void setCodigoAluno(Aluno codigoAluno) {
        this.codigoAluno = codigoAluno;
    }

    public Curso getCodigoCurso() {
        return codigoCurso;
    }

    public void setCodigoCurso(Curso codigoCurso) {
        this.codigoCurso = codigoCurso;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigo != null ? codigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CursoAluno)) {
            return false;
        }
        CursoAluno other = (CursoAluno) object;
        if ((this.codigo == null && other.codigo != null) || (this.codigo != null && !this.codigo.equals(other.codigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.CursoAluno[ codigo=" + codigo + " ]";
    }
    
}
