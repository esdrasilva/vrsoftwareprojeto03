/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.awt.HeadlessException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.swing.JOptionPane;
import model.Curso;
import util.JpaUtil;

/**
 *
 * @author BZR4
 */
public class CursoDAO {
    private static CursoDAO instance;
    protected EntityManager entityManager;
    
    public static CursoDAO getInstance(){
        if (instance == null) {
            return new CursoDAO();
        }
        return instance;
    }

    public CursoDAO() {
        this.entityManager = getEntityManager();
    }

    private EntityManager getEntityManager() {
        return JpaUtil.getEntityManagerFactory().createEntityManager();
    }
    
    public void persist(Curso curso) throws Exception{
        try{
            entityManager.getTransaction().begin();
            entityManager.persist(curso);
            entityManager.getTransaction().commit();        
        }catch(Exception e){
            showException(e);
            rollback();
        }finally{
            closeEntityManager();
        }
    }  
    
    public void remove(Curso curso){
        try{
            entityManager.getTransaction().begin();
            curso = entityManager.find(Curso.class, curso.getCodigo());
            entityManager.remove(curso);
            entityManager.getTransaction().commit();
        }catch(Exception e){
            showException(e);
            rollback();
        }finally{
            closeEntityManager();
        }
    }
    
     public void merge(Curso curso) {
         try {
            entityManager.getTransaction().begin();
            entityManager.merge(curso);
            entityManager.getTransaction().commit();
         } catch (Exception e) {
            showException(e);
            rollback();
        }finally{
            closeEntityManager();
        }
    }          
    
    public Curso findByCodigo(Integer codigo) {
        return getEntityManager().createNamedQuery(
                "Curso.findByCodigo",  Curso.class).setParameter("codigo", codigo).getSingleResult();                                               
    }
    
    public Curso findByDescricao(String codigo) {
        return getEntityManager().createNamedQuery(
                "Curso.findByDescricao",  Curso.class).setParameter("descricao", codigo).getSingleResult();                                               
    }
    
    public List<Curso> findAll(){
        return getEntityManager().createNamedQuery("Curso.findAll", Curso.class).getResultList();
    }
    
    private void closeEntityManager() {
        if (entityManager.isOpen()) {
            entityManager.close();
        }
    }

    private void rollback() {
        if (entityManager.isOpen()) {
            entityManager.getTransaction().rollback();
        }
    }

    private void showException(Exception e) throws HeadlessException {
        JOptionPane.showMessageDialog(null, String.format("Tipo de excecao: %s\nMensagem: %s", e.getClass().getSimpleName(), e.getMessage()));
    }
    
}
