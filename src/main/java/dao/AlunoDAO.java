/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.awt.HeadlessException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.swing.JOptionPane;
import model.Aluno;
import util.JpaUtil;

/**
 *
 * @author BZR4
 */
public class AlunoDAO {
    private static AlunoDAO instance;
    protected EntityManager entityManager;
    
    public static AlunoDAO getInstance(){
        if (instance == null) {
            return new AlunoDAO();
        }
        return instance;
    }

    public AlunoDAO() {
        this.entityManager = getEntityManager();
    }

    private EntityManager getEntityManager() {
        return JpaUtil.getEntityManagerFactory().createEntityManager();
    }
    
    public void persist(Aluno aluno) throws Exception{
        try{
            entityManager.getTransaction().begin();
            entityManager.persist(aluno);
            entityManager.getTransaction().commit();        
        }catch(Exception e){
            showException(e);
            rollback();
        }finally{
            closeEntityManager();
        }
    }  
    
    public void remove(Aluno aluno){
        try{
            entityManager.getTransaction().begin();
            aluno = entityManager.find(Aluno.class, aluno.getCodigo());
            entityManager.remove(aluno);
            entityManager.getTransaction().commit();
        }catch(Exception e){
            showException(e);
            rollback();
        }finally{
            closeEntityManager();
        }
    }
    
     public void merge(Aluno aluno) {
         try {
            entityManager.getTransaction().begin();
            entityManager.merge(aluno);
            entityManager.getTransaction().commit();
         } catch (Exception e) {
            showException(e);
            rollback();
        }finally{
            closeEntityManager();
        }
    }          
    
    public Aluno findByCodigo(Integer codigo) {
        return getEntityManager().createNamedQuery(
                "Aluno.findByCodigo",  Aluno.class)
                .setParameter("codigo", codigo).getSingleResult();
    }
    
    public List<Aluno> findAll(){
        return getEntityManager().createNamedQuery("Aluno.findAll", Aluno.class).getResultList();
    }
    
    private void closeEntityManager() {
        if (entityManager.isOpen()) {
            entityManager.close();
        }
    }

    private void rollback() {
        if (entityManager.isOpen()) {
            entityManager.getTransaction().rollback();
        }
    }

    private void showException(Exception e) throws HeadlessException {
        JOptionPane.showMessageDialog(null, String.format("Tipo de excecao: %s\nMensagem: %s", e.getClass().getSimpleName(), e.getMessage()));
    }
    
}
