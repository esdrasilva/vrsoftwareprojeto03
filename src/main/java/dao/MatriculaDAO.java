/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.awt.HeadlessException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.swing.JOptionPane;
import model.CursoAluno;
import util.JpaUtil;

/**
 *
 * @author BZR4
 */
public class MatriculaDAO {
    private static MatriculaDAO instance;
    protected EntityManager entityManager;
    
    public static MatriculaDAO getInstance(){
        if (instance == null) {
            return new MatriculaDAO();
        }
        return instance;
    }

    public MatriculaDAO() {
        this.entityManager = getEntityManager();
    }

    private EntityManager getEntityManager() {
        return JpaUtil.getEntityManagerFactory().createEntityManager();
    }
    
    public void persist(CursoAluno cursoAluno) throws Exception{
        try{
            entityManager.getTransaction().begin();
            entityManager.persist(cursoAluno);
            entityManager.getTransaction().commit();        
        }catch(Exception e){
            showException(e);
            rollback();
        }finally{
            closeEntityManager();
        }
    }  
    
    public void remove(CursoAluno cursoAluno){
        try{
            entityManager.getTransaction().begin();
            cursoAluno = entityManager.find(CursoAluno.class, cursoAluno.getCodigo());
            entityManager.remove(cursoAluno);
            entityManager.getTransaction().commit();
        }catch(Exception e){
            showException(e);
            rollback();
        }finally{
            closeEntityManager();
        }
    }
    
     public void merge(CursoAluno cursoAluno) {
         try {
            entityManager.getTransaction().begin();
            entityManager.merge(cursoAluno);
            entityManager.getTransaction().commit();
         } catch (Exception e) {
            showException(e);
            rollback();
        }finally{
            closeEntityManager();
        }
    }          
    
    public CursoAluno findByCodigo(Integer codigo) {
        return getEntityManager().createNamedQuery(
                "CursoAluno.findByCodigo",  CursoAluno.class).setParameter("codigo", codigo).getSingleResult();                                               
    }
    
    public List<CursoAluno> findAllByCodigo(Integer codigo) {
        return getEntityManager().createNamedQuery(
                "CursoAluno.findAllByCodigo",  CursoAluno.class).setParameter("codigo", codigo).getResultList();                                               
    }
    
    public List<CursoAluno> findAll(){
        return getEntityManager().createNamedQuery("CursoAluno.findAll", CursoAluno.class).getResultList();
    }
    
    private void closeEntityManager() {
        if (entityManager.isOpen()) {
            entityManager.close();
        }
    }

    private void rollback() {
        if (entityManager.isOpen()) {
            entityManager.getTransaction().rollback();
        }
    }

    private void showException(Exception e) throws HeadlessException {
        JOptionPane.showMessageDialog(null, String.format("Tipo de excecao: %s\nMensagem: %s", e.getClass().getSimpleName(), e.getMessage()));
    }
    
}
